(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(magit-merge-arguments (quote ("--ff-only")))
 '(package-selected-packages
   (quote
    (go-direx go-snippets go-stacktracer go-rename go-playground go-guru go-autocomplete go-mode magit auto-yasnippet smartparens undo-tree which-key multishell ido-vertical-mode smex dockerfile-mode yaml-mode markdown-mode tern-auto-complete tern js2-mode))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
