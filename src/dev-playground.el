;;; dev-playground.el --- Local dev playground for short snippets.

;; Copyright (C) 2016  johncming

;; Author: Alexander I.Grafov (axel) <grafov@gmail.com>
;; URL: https://github.com/grafov/dev-playground
;; Package-Version: 201701011
;; Keywords: tools,
;; Version: 0.1
;; Package-Requires: ((emacs "24"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;
;; `M-x dev-playground` and type you golang code then make&run it with `C-Return`.

;;

;;; Code:

(require 'time-stamp)

(defgroup dev-playground nil
  "Options specific to dev Playground."
  :group 'dev)

(defcustom dev-playground-basedir "~/dev/src/playground"
  "Base directory for playground snippets."
  :group 'dev-playground)

(defcustom dev-playground-ask-file-name nil
  "Non-nil means we ask for a name for the snippet."
  :type 'boolean
  :group 'dev-playground)

(defun dev-playground-snippet-unique-dir ()
  "Get unique directory"
  (let ((dir-name (concat dev-playground-basedir "/"
                          (time-stamp-string "at-%:y-%02m-%02d-%02H%02M%02S"))))
    (make-directory dir-name t)
    dir-name))

(defun dev-playground-snippet-file-name(&optional snippet-name)
    (concat (dev-playground-snippet-unique-dir) "/README.rst"))

;;;###autoload
(defun dev-playground ()
  "Run playground"
  (interactive)
  (let ((snippet-file-name (dev-playground-snippet-file-name)))
    (switch-to-buffer (create-file-buffer snippet-file-name))
    (insert "==============================

==============================
")
    (backward-char 32)
    (rst-mode)
    (set-visited-file-name snippet-file-name t)))

(provide 'dev-playground)
;;; dev-playground.el ends here
